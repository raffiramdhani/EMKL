@extends('pair.header')
@section('content')
@include('pair.PIB_modal')

  <title>Proccess | PIB</title>

@include('pair.sidebar')

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proses
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-copy"></i> Proses</li>
      </ol>
    </section>
    <!-- /Content Header -->

    
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
             <div class="box-header with-border">
                <h3 class="box-title">PIB</h3>
              
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <center>
                <table class="table table-striped table-hover" id="TablePIB" style="width: 100%;">
                  {{csrf_field()}}
                  <thead>
                    <tr>
                      <th></th>
                      <th>{{-- <button type="button" class="btn btn-success btn-sm" id="btnTambah">+</button> --}} No</th>
                      <th>No PIB</th>
                      <th>Tgl PIB</th>
                      <th>Klien</th>
                      <th>Type</th>
                      <th>Bea Masuk</th>
                      <th>PPn</th>
                      <th>BM TP</th>
                      <th>PPn TP</th>
                    </tr>
                  </thead>
                </table>
                </center> 
              </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('pair.fbutton')
@include('pair.footer')
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btnTambah').addClass('PIBpage');
    $('#proses-pib').parent('li').addClass('active').siblings('li').removeClass('active');
    $('#proses-menu').parent('li').addClass('active menu-open').siblings('li').removeClass('active menu-open');
  });
</script>
@endsection
