@extends('layouts.app')

@section('content')
@include('pair.profile_modal')

<div class="row">
    
    <style>
        a > img {        
                border: 1px transparent;
                border-radius: 50%; height: 100px;
                width: 100px;
            }
            a > img:hover {
                border-radius: 50%; height: 100px;
                width: 100px;
                background-color: gray;
                background-blend-mode: multiply;
            }
        </style>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <div class="panel-title text-center">
                            <a data-toggle="modal" data-target="#hisModal" style="text-decoration: none;">
                                    <img src="{{url($profiles->file_name)}}" alt="ppimg" style="box-shadow:0px 2px 6px #000;">
                                </a>
                            <h2><b>{{Auth::User()->name}}</b></h2>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="col-md-10 col-md-offset-1">
                            <!-- Trigger the modal with a button -->
                            <center>
                            <div class="panel panel-default col-sm-4 ">
                                <div class="panel-heading">Address</div>
                                    <div class="panel-body">
                                        <div>
                                            <h3></h3>
                                        </div>
                                        <div class="text-right"><a href="#">Edit</a></div>
                                    </div>
                            </div>
                            <div class="panel panel-default col-sm-4 ">
                                <div class="panel-heading">Job</div>
                                <div class="panel-body">
                                        <div>
                                            <h3></h3>
                                        </div>
                                        <div class="text-right"><a href="#">Edit</a></div>
                                    </div>
                            </div>
                            <div class="panel panel-default col-sm-4 ">
                                <div class="panel-heading">Level</div>
                                <div class="panel-body">
                                        <div class="text-justify">
                                            <h3></h3>
                                        </div>
                                        <div class="text-right"><a href="#">Edit</a></div>
                                    </div>
                            </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection