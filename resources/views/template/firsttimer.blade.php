<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>SetUp Profile | BukuTamu</title>
	<link rel="shortcut icon" href="{{ asset('favicon.ico/favicon.ico') }}" >
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script src="{{ asset('js/app.js') }}"></script>
</head>
<body style="background-color: #f9f9f9;">
<div class="app">
	<div class="container" style="margin-top: 20px;">
		<div class="row jumbotron" style="background-color: white;" id="agreeying">
			<div class="col-md-12" style="text-align: center;">
				<h1 style="margin-bottom: 100px;">Setup Profile</h1>
				<div class="form-group">
	                <div class="col-md-6 col-md-offset-3">
	                	<h3>Wanna Proceed To Set Up your Profile ?</h3>
	                    <button class="btn btn-primary" id="agree" style="width: 90px; height: 34px; font-size: 12px;">
	                                    OK
                        </button>
                        <a href="/home" class="btn btn-danger" style="width: 90px; height: 34px; font-size: 12px;">
	                                    Later
                        </a>
	                </div>
	            </div>
			</div>
		</div>
		<div id="agreed">
			@include('template.agree')
		</div>
	</div>
</div>
<script type="text/javascript" src="/js/control-backbone.js"></script>
</body>
</html>