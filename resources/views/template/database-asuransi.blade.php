@extends('pair.header')
@section('content')
@include('pair.Asuransi_modal')

  <title>Data Master | Asuransi</title>

@include('pair.sidebar')

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Master
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-database"></i> Data Master</li>
      </ol>
    </section>
    <!-- /Content Header -->

    
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
             <div class="box-header with-border">
                <h3 class="box-title">Data Asuransi</h3>
              
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <center>
                <table class="table table-striped table-hover" id="TableAsuransi" style="width: 100%;">
                  {{csrf_field()}}
                  <thead>
                    <tr>
                      <th></th>
                      <th>No</th>
                      <th>Nama Asuransi</th>
                      <th>NPWP</th>
                      <th>Penganggung Jwb</th>
                      <th>Jabatan</th>
                      <th>Telp</th>
                      <th>Fax</th>
                    </tr>
                  </thead>
                </table>
                </center> 
              </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('pair.fbutton')
@include('pair.footer')
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btnTambah').addClass('Asuransipage');
    $('#data-asur').parent('li').addClass('active').siblings('li').removeClass('active');
    $('#data-menu').parent('li').addClass('active menu-open').siblings('li').removeClass('active menu-open'); 
  });
</script>

@endsection
