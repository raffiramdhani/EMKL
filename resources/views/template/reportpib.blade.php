@extends('pair.header')
@section('content')

  <title>Proccess | IN</title>

@include('pair.sidebar')

<!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proses
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-home"></i> Home</a></li>
        <li class="active"><i class="fa fa-refresh"></i> Proses</li>
      </ol>
    </section>
    <!-- /Content Header -->

    
    <!-- Main content -->
    <section class="content">
      
      <div class="row">
        <div class="col-md-12">
          <div class="box">
             <div class="box-header with-border">
                <h3 class="box-title">Guest List</h3>
              
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <div class="btn-group">
                    <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                      <i class="fa fa-wrench"></i></button>
                    <ul class="dropdown-menu" role="menu">
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </div>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <center>
                <table class="table table-striped table-hover" id="TableOne" style="width: 100%;">
                  {{csrf_field()}}
                  <thead>
                    <tr>
                      <th>{{-- <button type="button" class="btn btn-success btn-sm" id="btnTambah">+</button> --}} No</th>
                      <th>Nama</th>
                      <th>Check In</th>
                      <th>Check Out</th>
                      <!-- <th>Option</th> -->
                    </tr>
                  </thead>
                </table>
                </center> 
              </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@include('pair.footer')
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('#btnTambah').addClass('RepPIBpage');
    $('#report-pib').parent('li').addClass('active').siblings('li').removeClass('active');
    $('#report-menu').parent('li').addClass('active menu-open').siblings('li').removeClass('active menu-open');
  });
</script>

@endsection
