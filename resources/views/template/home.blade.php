@extends('pair.header')
@section('content')

  <title>Admin Page | DashBoard</title>

@include('pair.sidebar')

<!-- Content Wrapper. Contains page content -->

	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Dashboard
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
			</ol>
		</section>
		<!-- /Content Header -->

		
		<!-- Main content -->
		<section class="content">
			<!-- Info boxes -->
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-body text-center">
							<h1>Welcome, {{Auth::user()->name}}</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-aqua"><i class="ion ion-ios-people"></i></span>

						<div class="info-box-content">
							<span class="info-box-text">Klien</span>
							<span class="info-box-number"><div id="tamuCounter">{{$Klien->count()}}</div></span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-red"><i class="fa fa-bank"></i></span>

						<div class="info-box-content">
							<span class="info-box-text">PIB</span>
							<span class="info-box-number">{{$PIB->count()}}</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->

				<!-- fix for small devices only -->
				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-green"><i class="ion ion-ios-paper"></i></span>

						<div class="info-box-content">
							<span class="info-box-text">Asuransi</span>
							<span class="info-box-number">{{$Asuransi->count()}}</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->
				<div class="col-md-3 col-sm-6 col-xs-12">
					<div class="info-box">
						<span class="info-box-icon bg-yellow"><i class="fa fa-user"></i></span>

						<div class="info-box-content">
							<span class="info-box-text">User</span>
							<span class="info-box-number" id="jumlah">{{$User->count()}}</span>
						</div>
						<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">User Web</h3>
						
							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
								<div class="btn-group">
									<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-wrench"></i></button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
									</ul>
								</div>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<center>
							<table class="table table-striped table-hover" id="TableUser" style="width: 100%;">
								{{csrf_field()}}
								<thead>
									<tr>
										<th></th>
										<th>{{-- <button type="button" class="btn btn-success btn-sm" id="btnTambah">+</button> --}} No</th>
										<th>Nama User</th>
										<th>Email</th>
										<th>Jenis User</th>
										<th>status</th>
										<th>Foto</th>
									</tr>
								</thead>
							</table>
							</center>	
						</div>
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
	</div>
	<!-- /.content-wrapper -->
@include('pair.footer')
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#pageSelector').val('home');
		$('#btnTambah').removeClass('infoTambah').addClass('homeTambah');
		$('#home-menu').addClass('active');
	});
</script>

@endsection
