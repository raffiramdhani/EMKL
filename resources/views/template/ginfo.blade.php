@extends('pair.header')
@section('content')
<title>Admin Page | Data Master</title>
@include('pair.sidebar')

<div class="content-wrapper">
	<section class="content-header">
			<h1>
				Data Master
			</h1>
			<ol class="breadcrumb">
				<li><a href="{{url('/')}}"><i class="fa fa-home"></i> Home</a></li>
				<li class="active"><i class="fa fa-info"></i> Data Master</li>
			</ol>
		</section>
		<section class="content">
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Guest List</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
								<div class="btn-group">
									<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-wrench"></i></button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
									</ul>
								</div>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<center>
								<table class="table table-striped table-hover" id="TableTwo" style="width: 100%;text-align: center;">
										{{csrf_field()}}
										<thead>
											<tr>
												<th>NO</th>
												<th>Nama Tamu</th>
												<th>Kontak</th>
												<th>Alamat</th>
												<th>Action</th>
											</tr>
									</thead>
									</table>
							</center>
						</div>
					</div>
					<!-- /.box -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">Guest Information</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
								<div class="btn-group">
									<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-wrench"></i></button>
									<ul class="dropdown-menu" role="menu">
										<li><a href="#">Action</a></li>
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
									</ul>
								</div>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<center>
								<table class="table table-striped table-hover" id="TableTwov2" style="width: 100%;text-align: center;">
										{{csrf_field()}}
										<thead>
											<tr>
												<th>NO</th>
												<th>Nama Tamu</th>
												<th>Type Kamar</th>
												<th>Check In</th>
												<th>Check Out</th>
												<th>Option</th>
											</tr>
									</thead>
									</table>
							</center>
						</div>
					</div>
					<!-- /.box -->
				</div>
			</div>
		</section>
	</div>
@include('pair.fbutton')
@include('pair.footer')
@endsection