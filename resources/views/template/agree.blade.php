<div class="row jumbotron" style="margin-top: 10px;background-color: white;">
        <div class="col-md-12" style="text-align: center;">
          <h1 style="margin-bottom: 50px;">Setup Profile</h1>
          <form class="form-horizontal" name="profile" enctype="multipart/form-data" method="POST" action="/profUp/{{Auth::User()->id}}">
          	{{ csrf_field() }}
          	<div class="form-group">
          		<label for="name" class="col-md-4 control-label" style="font-size: 25px;">Name</label>
          		<div class="col-md-6">
          			<input id="name" type="text" name="name" class="form-control text-center" value="{{Auth::User()->name}}"> 
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="mail" class="col-md-4 control-label" style="font-size: 25px;">E-mail</label>
          		<div class="col-md-6">
          			<input id="mail" type="text" name="mail" class="form-control text-center" value="{{Auth::User()->email}}"> 
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="pass" class="col-md-4 control-label" style="font-size: 25px;">Password</label>
          		<div class="col-md-6">
          			<p id="pass" type="text" name="pass" class="form-control text-center"><i>User Privacy</i></p> 
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="address" class="col-md-4 control-label" style="font-size: 25px;">Address</label>
          		<div class="col-md-6">
          			<textarea id="address" name="address" class="form-control text-center" style="height: 70px;"></textarea>
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="edu" class="col-md-4 control-label" style="font-size: 25px;">Education</label>
          		<div class="col-md-6">
          			<input id="edu" type="text" name="edu" class="form-control text-center">
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="job" class="col-md-4 control-label" style="font-size: 25px;">Job(s)</label>
          		<div class="col-md-6">
          			<input id="job" type="text" name="job" class="form-control text-center">
          		</div>
          	</div>
          	<div class="form-group">
          		<label for="image" class="col-md-4 control-label" style="font-size: 25px;">Profile Image</label>
          		<div class="col-md-6">
          			<input id="image" type="file" name="image" class="form-control" accept=".jpg, .JPEG, .bmp, .png">
          		</div>
          	</div>
          	<div class="form-group">
	                            <div class="col-md-6 col-md-offset-3">
	                                <button type="submit" class="btn btn-primary" style="width: 90px; height: 34px; font-size: 12px;">
	                                    Save
	                                </button>
	                                <a href="/home" class="btn btn-danger" style="width: 90px; height: 34px; font-size: 12px;">
	                                    Cancel
	                                </a>
	                            </div>
	                        </div>
          </form>
        </div>
      </div>