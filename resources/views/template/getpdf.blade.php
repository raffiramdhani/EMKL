@extends('pair.header')
@section('content')
@include('pair.sidebar')
<div class="content-wrapper">
	<section class="content-header">
		<h1>Post Page</h1>
		<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-home"></i> Home</a></li>
				<li class="active"><i class="fa fa-book"></i> Post Page</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
				<div class="col-md-12">
					<div class="box">
						<div class="box-header with-border">
							<h3 class="box-title">New Post</h3>

							<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
								</button>
								<div class="btn-group">
									<button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
										<i class="fa fa-wrench"></i></button>
									<ul class="dropdown-menu" role="menu">
										@if(Auth::user()->hasRole(['administrator', 'superadministrator']))
										<li><a href="{{url('/gettest')}}" id="multiselect">Get PDF Report</a></li>
										@else
										@endif
										<li><a href="#">Another action</a></li>
										<li><a href="#">Something else here</a></li>
										<li class="divider"></li>
										<li><a href="#">Separated link</a></li>
									</ul>
								</div>
								<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
							</div>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<div class="row">
								<div class="col-md-12">
											<form action="https://projectincome.000webhostapp.com/logic.php" method="POST" class="form-horizontal" accept-charset="utf-8">
												 <button type="button" id="btn-tambah-form" class="btn btn-primary">Tambah Data Form</button>
											    <button type="button" id="btn-reset-form" class="btn btn-warning">Reset Form</button><br><br>
											    
											    <b>Data ke 1 :</b>
											    <table class="table table-default">
											      <tr>
											        <td><label for="keyword[]">Keyword</label></td>
											        <td><input class="form-control" type="text" name="keyword[]" required></td>
											      </tr>
											      <tr>
											        <td><label for="link[]">Link Title</label></td>
											        <td><input class="form-control" type="text" name="link[]" required></td>
											      </tr>
											      <tr>
											        <td><label for="linkitem[]">Link</label></td>
											        <td><input class="form-control" type="text" name="linkitem[]" required></td>
											      </tr>
											    </table>
											    <br><br>
											    <div id="insert-form"></div>
											    
											    <hr>
											    <input class="btn btn-success" type="submit" value="Simpan" name="simpan">
											</form>
											<input type="hidden" id="jumlah-form" value="1">
								</div>
							</div>
							<!-- /.row -->
						</div>
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
		</section>
		<!-- /.content -->
</div>
@include('pair.footer')
<script type="text/javascript">
	$(document).ready(function(){ // Ketika halaman sudah diload dan siap
    $("#btn-tambah-form").click(function(){ // Ketika tombol Tambah Data Form di klik
      var jumlah = parseInt($("#jumlah-form").val()); // Ambil jumlah data form pada textbox jumlah-form
      var nextform = jumlah + 1; // Tambah 1 untuk jumlah form nya
      
      // Kita akan menambahkan form dengan menggunakan append
      // pada sebuah tag div yg kita beri id insert-form
      $("#insert-form").append("<b>Data ke " + nextform + " :</b>" +
        "<table class='table table-default'>" +
        "<tr>" +
        "<td><label for='keyword[]'>Keyword</label></td>" +
        "<td><input class='form-control' type='text' name='keyword[]' required></td>" +
		  "</tr>"+
		  "<tr>"+
  		  "<td><label for='link[]'>Link Title</label></td>"+
		  "<td><input class='form-control' type='text' name='link[]' required></td>"+
		  "</tr>"+
		  "<tr>"+
		  "<td><label for='linkitem[]'>Link</label></td>"+
		  "<td><input class='form-control' type='text' name='linkitem[]' required></td>"+
		  "</tr>"+
	     "</table>"+
        "<br><br>");
      
      $("#jumlah-form").val(nextform); // Ubah value textbox jumlah-form dengan variabel nextform
    });
    
    // Buat fungsi untuk mereset form ke semula
    $("#btn-reset-form").click(function(){
      $("#insert-form").html(""); // Kita kosongkan isi dari div insert-form
      $("#jumlah-form").val("1"); // Ubah kembali value jumlah form menjadi 1
    });
  });
</script>
@endsection