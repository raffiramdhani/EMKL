<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">TAMBAH DATA</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myForm">
					{{csrf_field()}}
					<div style="text-align: center;">
						<br>
						<h5 type="text" class="hapusDialog">Hapus Data?</h5>
						<div class="form-group inputan">
							<label for="NamaTamu" class="col-lg-2 control-label inputan">Nama Tamu</label>
							<div class="col-md-10">
									<select class="form-control" id="select_id" style="width: 450px">
										<option>Nama Tamu</option>
										@foreach($tamus as $tamu)
											<option data-id="{{$tamu->id}}" data-alamat="{{$tamu->Alamat}}">{{$tamu->NamaTamu}}</option>
										@endforeach
									</select>
							</div>
						</div>
						<div class="form-group inputan">
							<label for="Keperluan" class="col-lg-2 control-label inputan">Keperluan</label>
							<div class="col-md-10">
								<input type="text" style="width: 450px;" class="form-control inputan" name="Keperluan" id="Keperluan" placeholder="Jenis Keperluan">
								<span id="kontak_error"></span>
							</div>
						</div>
						<div class="form-group inputan">
							<label for="Alamat" class="col-lg-2 control-label inputan">Alamat</label>
							<div class="col-md-10">
								<textarea class="form-control inputan" style="width: 450px;" name="Alamat" id="Alamat" placeholder="Alamat"></textarea>
								<span id="alamat_error"></span>
							</div>
						</div>
						<div class="form-group">
								<button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
								<button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
								<input type="hidden" name="ident" id="ident" value="">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
