<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">TAMBAH DATA</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myForm">
				{{csrf_field()}}
					<div style="text-align: center;">
								<br>
								<h5 type="text" class="hapusDialog">Hapus Data?</h5>
						<div class="form-group inputTambah">
							<label for="NoCB" class="col-md-3 control-label inputTambah">No.Custom Bond</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" id="noCB" name="NoCB" placeholder="No.Custom Bond">
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambahDate">
							<label for="TglCB" class="col-md-3 control-label inputTambahDate">Tanggal CB</label>
							<div class="col-md-4">
								<input type="date" style="width: 400px;" class="form-control inputTambahDate" name="TglCB" id="TanggalCBDate">
							</div>
						</div>
						<div class="form-group inputTambahText">
							<label for="TglCB" class="col-md-3 control-label inputTambahText">Tanggal CB</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambahText" name="TglCB" id="tanggalCBText">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label class="col-md-3 control-label inputTambah">No.PIB</label>
							<div class="col-md-4">
								<select class="form-control inputTambah" style="width:400px;" id="idPIB">
									<option id="identifier">No.PIB</option>
									@foreach($PIB as $pib)
									<option data-id="{{$pib->id_pib}}">{{$pib->no_pib}}</option>
									@endforeach
								</select>
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Blanko" class="col-md-3 control-label inputTambah">Blanko</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="Blanko" id="Blanko" placeholder="Blanko">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Kegiatan" class="col-md-3 control-label inputTambah">Kegiatan</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="Kegiatan" id="kegiatan" placeholder="Kegiatan">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Jumlah" class="col-md-3 control-label inputTambah">Jumlah</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="Jumlah" id="jumlah" placeholder="Jumlah">
							</div>
						</div>
					
						<div class="form-group">
								<button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
								<button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
								<input type="hidden" name="ident" id="ident" value="">
						</div>
					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
