<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">TAMBAH DATA</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myForm">
				{{csrf_field()}}
					<div style="text-align: center;">
								<br>
								<h5 type="text" class="hapusDialog">Hapus Data?</h5>
						<div class="form-group inputTambah">
							<label for="Nama" class="col-md-3 control-label inputTambah">Nama Asuransi</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" id="nama_asuransi" name="Nama" placeholder="Nama Asuransi">
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Penanggung" class="col-md-3 control-label inputTambah">Penanggung Jawab</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="Penanggung" id="png_jwb" placeholder="Penanggung Jawab">
								{{-- <span id="kontak_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Jabatan" class="col-md-3 control-label inputTambah">Jabatan</label>
							<div class="col-md-4">
								<input type="text" class="form-control inputTambah" style="width: 400px;" name="Jabatan" id="jabatan" placeholder="Jabatan">
								{{-- <span id="alamat_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="npwp" class="col-md-3 control-label inputTambah">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control inputTambah" style="width: 400px;" name="npwp" id="npwp" placeholder="No.NPWP">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="alamat" class="col-md-3 control-label inputTambah">Alamat</label>
							<div class="col-md-4">
								<textarea style="width: 400px;height: 100px;" class="form-control inputTambah" name="alamat" id="alamat">
								</textarea>
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="kota" class="col-md-3 control-label inputTambah">Kota</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="kota" id="kota" placeholder="Kota">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="telepon" class="col-md-3 control-label inputTambah">Telepon</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="telepon" id="telepon" placeholder="Telepon">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="faksimili" class="col-md-3 control-label inputTambah">Faksimili</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="faksimili" id="faksimili" placeholder="Faksimili">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="email" class="col-md-3 control-label inputTambah">E-mail</label>
							<div class="col-md-4">
								<input type="email" style="width: 400px;" class="form-control inputTambah" name="email" id="email" placeholder="E-mail">
							</div>
						</div>
						<div class="form-group">
								<button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
								<button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
								<input type="hidden" name="ident" id="ident" value="">
						</div>
					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
