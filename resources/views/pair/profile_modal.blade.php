<!-- Modal -->
        <div id="hisModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UBAH PHOTO PROFILE</h4>
              </div>
              <div class="modal-body">
                <form method="POST" action="{{url('/store')}}" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <label for="image" class="col-md-2">Photo</label>
                  <input type="file" name="image" class="form-control">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </form>
              </div>
            </div>

          </div>
        </div>
    <!--End of Modal -->
    