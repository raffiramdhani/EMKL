<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">TAMBAH DATA</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myForm">
				{{csrf_field()}}
					<div style="text-align: center;">
								<br>
								<h5 type="text" class="hapusDialog">Hapus Data?</h5>
						<div class="form-group inputTambah">
							<label for="Klien" class="col-md-3 control-label inputTambah">Klien</label>
							<div class="col-md-4">
								<select class="form-control inputTambah" style="width:400px;" id="klien">
									<option id="identifier">Nama Klien</option>
									@foreach($Klien as $klien)
									<option data-id="{{$klien->id_cln}}">{{$klien->nama_cln}}</option>
									@endforeach
								</select>
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="NoPIB" class="col-md-3 control-label inputTambah">No. PIB</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" id="noPIB" name="NoPIB" placeholder="No.PIB">
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambahDate">
							<label for="TglPIB" class="col-md-3 control-label inputTambahDate">Tanggal PIB</label>
							<div class="col-md-4">
								<input type="date" style="width: 400px;" class="form-control inputTambahDate" name="TglPIB" id="TglPIBDate">
							</div>
						</div>
						<div class="form-group inputTambahText">
							<label for="TglPIB" class="col-md-3 control-label inputTambahText">Tanggal PIB</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambahText" name="tglPIB" id="TglPIBText">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="Type" class="col-md-3 control-label inputTambah">Type</label>
							<div class="col-md-4">
								<input type="text" class="form-control inputTambah" style="width: 400px;" name="Type" id="type" placeholder="Type">
								{{-- <span id="alamat_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="BeaMasuk" class="col-md-3 control-label inputTambah">Bea Masuk</label>
							<div class="col-md-4">
								<input type="text" class="form-control inputTambah" style="width: 400px;" name="BeaMasuk" id="beaMasuk" placeholder="Bea Masuk">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="PPn" class="col-md-3 control-label inputTambah">PPn</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="PPn" id="ppn" placeholder="PPn">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="BMTP" class="col-md-3 control-label inputTambah">BM TP</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="BMTP" id="bmTP" placeholder="BM TP">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="PPnTP" class="col-md-3 control-label inputTambah">PPn TP</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="PPnTP" id="ppnTP" placeholder="PPn TP">
							</div>
						</div>
					
						<div class="form-group">
								<button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
								<button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
								<input type="hidden" name="ident" id="ident" value="">
						</div>
					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
