<!-- Modal -->

<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">TAMBAH DATA</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" id="myForm">
				{{csrf_field()}}
					<div style="text-align: center;">
								<br>
								<h5 type="text" class="hapusDialog">Hapus Data?</h5>
						<div class="form-group inputTambah">
							<label for="Nama" class="col-md-3 control-label inputTambah">Nama Klien</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" id="nama_klien" name="Nama" placeholder="Nama klien">
								{{-- <span id="nama_error" class="label label-danger"></span> --}}
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="npwp" class="col-md-3 control-label inputTambah">NPWP</label>
							<div class="col-md-4">
								<input type="text" class="form-control inputTambah" style="width: 400px;" name="npwp" id="npwp" placeholder="No.NPWP">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="niper" class="col-md-3 control-label inputTambah">NIPER</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="niper" id="niper" placeholder="No.NIPER">
							</div>
						</div>
						<div class="form-group inputTambahDate">
							<label for="tglSK" class="col-md-3 control-label inputTambah">Tanggal SK</label>
							<div class="col-md-4">
								<input type="date" style="width: 400px;" class="form-control inputTambahDate" name="tglSK" id="tanggalSK">
							</div>
						</div>
						<div class="form-group inputTambahText">
							<label for="tglSK" class="col-md-3 control-label inputTambah">Tanggal SK</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambahText" name="tglSK" id="tanggalSK">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="alamat" class="col-md-3 control-label inputTambah">Alamat</label>
							<div class="col-md-4">
								<textarea style="width: 400px;height: 100px;" class="form-control inputTambah" name="alamat" id="alamat">
								</textarea>
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="kota" class="col-md-3 control-label inputTambah">Kota</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="kota" id="kota" placeholder="Kota">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="telepon" class="col-md-3 control-label inputTambah">Telepon</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="telepon" id="telepon" placeholder="Telepon">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="faksimili" class="col-md-3 control-label inputTambah">Faksimili</label>
							<div class="col-md-4">
								<input type="text" style="width: 400px;" class="form-control inputTambah" name="faksimili" id="faksimili" placeholder="Faksimili">
							</div>
						</div>
						<div class="form-group inputTambah">
							<label for="email" class="col-md-3 control-label inputTambah">E-mail</label>
							<div class="col-md-4">
								<input type="email" style="width: 400px;" class="form-control inputTambah" name="email" id="email" placeholder="Email Klien">
							</div>
						</div>
					
						<div class="form-group">
								<button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
								<button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
								<input type="hidden" name="ident" id="ident" value="">
						</div>
					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
