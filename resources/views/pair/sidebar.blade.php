<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div id="sdbar" class="">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li id="home-menu">
          <a href="{{url('/dashboard')}}" >
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Data Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span> 
          </a>
          <ul class="treeview-menu" id="data-menu">
            <li><a href="{{url('/database/asuransi')}}" id="data-asur"><i class="fa fa-certificate"></i> Asuransi</a></li>
            <li><a href="{{url('/database/klien')}}" id="data-klien"><i class="fa fa-users"></i> Klien</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-copy"></i> <span>Proses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" id="proses-menu">
            <li><a href="{{url('/proccess/pib')}}" id="proses-pib"><i class="fa fa-circle-o"></i> P I B</a></li>
            <li><a href="{{url('/proccess/cb')}}" id="proses-cb"><i class="fa fa-circle-o"></i> Custom Bond</a></li>
            <li><a href="{{url('/proccess/pl')}}" id="proses-pl"><i class="fa fa-circle-o"></i> Placing</a></li>
            <li><a href="{{url('/proccess/in')}}" id="proses-in"><i class="fa fa-circle-o"></i> Invoice</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-print"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" id="report-menu">
            <li><a href="{{url('/report/pib')}}" id="report-pib"><i class="fa fa-circle-o"></i> P I B</a></li>
            <li><a href="{{url('/report/cb')}}" id="report-cb"><i class="fa fa-circle-o"></i> Custom Bond</a></li>
            <li><a href="{{url('/report/pl')}}" id="report-pl"><i class="fa fa-circle-o"></i> Placing</a></li>
            <li><a href="{{url('/report/in')}}" id="report-in"><i class="fa fa-circle-o"></i> Invoice</a></li>
          </ul>
        </li>
        <li>
          <a href="{{url('/setting')}}">
            <i class="fa fa-cog"></i> <span>Pengaturan</span>
          </a>
        </li>
      </ul>
    </div>
    </section>
    <!-- /.sidebar -->
  </aside>


  