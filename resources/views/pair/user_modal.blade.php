<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">EDIT USER</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" id="myForm">
          {{csrf_field()}}
          <div style="text-align: center;">
            <br>
            <h5 type="text" class="hapusDialog">Hapus Data?</h5>
            <div class="form-group inputan">
              <label for="name" class="col-lg-2 control-label inputan">Nama</label>
              <div class="col-md-10">
                @if(Auth::user()->hasRole('superadministrator'))
                <input type="text" style="width: 450px;" class="form-control inputan" id="name" name="name" value="">
                @else
                <input type="text" style="width: 450px;" class="form-control inputan" id="name" name="name" value="" disabled>
                @endif
                <span id="nama_error" class="label label-danger"></span>
              </div>
            </div>
            <div class="form-group inputan">
              <label for="mail" class="col-lg-2 control-label inputan">E-mail</label>
              <div class="col-md-10">
                @if(Auth::user()->hasRole('superadministrator'))
                <input type="text" style="width: 450px;" class="form-control inputan" id="mail" name="mail" value="">
                @else
                <input type="text" style="width: 450px;" class="form-control inputan" name="mail" id="mail" value="" disabled>
                @endif
                <span id="kontak_error" class="label label-danger"></span>
              </div>
            </div>
            <div class="form-group inputan">
              <label for="role" class="col-lg-2 control-label inputan">Role</label>
              <div class="col-md-10">
                  <select class="form-control" id="select_id" style="width: 450px">
                    <option value=""></option>
                    @foreach($roles as $role)
                      @if($role->name == "superadministrator")
                      @else
                      <option value="{{$role->id}}">{{$role->name}}</option>
                      @endif
                    @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group">
                <button class="btn btn-default" id="dismiss" data-dismiss="modal">CLOSE</button>
                <button type="submit" class="btn btn-primary" id="btnSave" value="add">SUBMIT</button>
                <input type="hidden" name="ident" id="ident" value="">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
