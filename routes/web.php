<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'ViewsController@show');

Route::get('/testter', function(){
	return Terbilang::make(100000);
});

Route::get('/posttest', function(){ return view('template.post'); });

Route::get('/get2', 'UserController@getData3')->middleware('AdminAuth');

Route::get('/database/{type}', 'ViewsController@database')->middleware('AdminAuth');

Route::get('/dashboard' , 'ViewsController@index')->middleware('AdminAuth');

Route::get('/profile/{id}', 'UserController@profile')->middleware('AdminAuth');

Route::get('/proccess/{type}', 'ViewsController@proccess')->middleware('AdminAuth');

Route::get('/coba', 'ViewsController@getData');

Route::get('/getreport', 'ViewsController@getreport');

Route::get('/app', function(){return view('app');});

Route::get('/firsttimer', function(){ return view('template.firsttimer'); });

Route::get('/coba2', 'ViewsController@getData2');

Route::get('/up', 'ViewsController@upview');

Route::get('/report/{type}', 'ViewsController@report')->middleware('AdminAuth');

Route::get('/setting', 'ViewsController@setting')->middleware('AdminAuth');

Route::get('/getPIB', 'ViewsController@getPIBData');

Route::get('/getKlien', 'ViewsController@getClient');

Route::get('/getAsuransi', 'ViewsController@getAsuransi');

Route::get('/reportPL', 'PDFController@ReportPL');

Route::get('/reportPIB', 'PDFController@ReportPIB');

Route::get('/getUser', 'UserController@user_index');

Route::get('/getCB', 'ViewsController@getCB');

Route::post('/insert', 'CrudController@add');

Route::post('/insert2', 'CrudController@add2');

Route::post('/insert3', 'CrudController@add3');

Route::post('/insert4', 'CrudController@add4');

Route::post('/profUp/{id}', 'UserController@create_profile');

Route::post('/store', 'PDFController@ReportCB');

Route::post('/postCB', 'PDFController@ReportCB');

Route::patch('/change/{id}', 'CrudController@change');

Route::patch('/change2/{id}', 'CrudController@change2');

Route::patch('/change3/{id}' ,'CrudController@change3');

Route::delete('/hapus/{id}', 'CrudController@hapus');

Route::delete('/hapus2/{id}', 'CrudController@hapus2');

Route::delete('/hapus3/{id}', 'CrudController@hapus3');
