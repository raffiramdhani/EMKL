<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Profile;
use App\PIB;
use App\Klien;
use App\Asuransi;
use DB;
use Auth;
use Carbon\Carbon;

class CrudController extends Controller
{
    public function add(Request $req){
    	// $date = Carbon::parse($req->tanggalSK)->format('d-M-Y');
    	$baru = new PIB;
    	$baru->no_pib = $req->noPIB;
    	$baru->tgl_pib = $req->tanggalPIB;
    	$baru->type = $req->type;
    	$baru->bea_masuk = $req->beaMasuk;
    	$baru->ppn = $req->ppn;
    	$baru->bm_tp = $req->bmTP;
    	$baru->ppn_tp = $req->ppnTP;
    	$baru->id_cln = $req->klien;
    	$baru->save();
		return response()->json($baru);
	}

	public function add2(Request $req){
		$klien = new Klien;
		$klien->nama_cln = $req->nama_klien;
		$klien->npwp_cln = $req->npwp;
		$klien->niper = $req->niper;
		$klien->tgl_sk = $req->tanggalSK;
		$klien->alamat_cln = $req->alamat;
		$klien->kota_cln = $req->kota;
		$klien->telp_cln = $req->telepon;
		$klien->fax_cln = $req->faksimili;
		$klien->email_cln = $req->email;
		$klien->save();
		return response()->json($klien);
	}

	public function add3(Request $req){
		$asrs = new Asuransi;
		$asrs->nama_asrs = $req->nama_asrs;
		$asrs->png_jwb = $req->png_jwb;
		$asrs->jabatan = $req->jabatan;
		$asrs->npwp_asrs = $req->npwp;
		$asrs->alamat_asrs = $req->alamat;
		$asrs->kota_asrs = $req->kota;
		$asrs->telp_asrs = $req->telepon;
		$asrs->fax_asrs = $req->faksimili;
		$asrs->email_asrs = $req->email;
		$asrs->save();
		return response()->json($asrs);
	}

	public function add4(Request $req){
		$cb = DB::table('cbs')->insert([
			'no_cb' => $req->noCB,
			'tgl_cb' => $req->tanggalCB,
			'id_pib' => $req->idPIB,
			'kegiatan' => $req->kegiatan,
			'no_blanko' => $req->blanko,
			'jumlah' => $req->jumlah,
		]); 
		return response()->json($cb);
	}
	
	public function change(Request $req, $id){
		// $date = Carbon::parse($req->tanggalSK)->format('d-m-Y');
		if($req->tanggalPIB != ""){
			$baru = PIB::where('id_pib', $id)->update([
				'no_pib' => $req->noPIB,
		    	'tgl_pib' => $req->tanggalPIB,
		    	'type' => $req->type,
		    	'bea_masuk' => $req->beaMasuk,
		    	'ppn' => $req->ppn,
		    	'bm_tp' => $req->bmTP,
		    	'ppn_tp' => $req->ppnTP,
			]);
		}else{
			$baru = PIB::where('id_pib', $id)->update([
			'no_pib' => $req->noPIB,
	    	'type' => $req->type,
	    	'bea_masuk' => $req->beaMasuk,
	    	'ppn' => $req->ppn,
	    	'bm_tp' => $req->bmTP,
	    	'ppn_tp' => $req->ppnTP,
		]);
		}
		return response()->json($baru);
	}

	public function change2(Request $req, $id){
		if($req->tanggalSK != ""){
			$data = Klien::where('id_cln', $id)->update([
			'nama_cln' => $req->nama_klien,
	    	'npwp_cln' => $req->npwp,
	    	'niper' => $req->niper,
	    	'tgl_sk' => $req->tanggalSK,
	    	'alamat_cln' => $req->alamat,
	    	'kota_cln' => $req->kota,
	    	'telp_cln' => $req->telepon,
	    	'fax_cln' => $req->faksimili,
	    	'email_cln' => $req->email,
		]);
		}else{
			$data = Klien::where('id_cln', $id)->update([
				'nama_cln' => $req->nama_klien,
		    	'npwp_cln' => $req->npwp,
		    	'niper' => $req->niper,
		    	'alamat_cln' => $req->alamat,
		    	'kota_cln' => $req->kota,
		    	'telp_cln' => $req->telepon,
		    	'fax_cln' => $req->faksimili,
		    	'email_cln' => $req->email,
			]);
		}
		return response()->json($data);
	}

	public function change3(Request $req, $id){
		$data = Asuransi::where('id_asrs', $id)->update([
			'nama_asrs' => $req->nama_asrs,
			'png_jwb' => $req->png_jwb,
			'jabatan' => $req->jabatan,
	    	'npwp_asrs' => $req->npwp,
	    	'alamat_asrs' => $req->alamat,
	    	'telp_asrs' => $req->telepon,
	    	'fax_asrs' => $req->faksimili,
	    	'email_asrs' => $req->email,
		]);
		return response()->json($data);
	}
	
	public function hapus($id){
		PIB::where('id_pib',$id)->delete();
		return response()->json();
	}

	public function hapus2($id){
		Klien::where('id_cln', $id)->delete();
		return response()->json();
	}

	public function hapus3($id){
		Asuransi::where('id_asrs', $id)->delete();
		return response()->json();
	}

	public function store(Request $request){
		// if($request->hasFile('image')){
  //     $image = $request->file('image');
  //     $path = public_path().'/images/';
  //     $filename = time().'.'.$image->getClientOriginalExtension();
  //     $image->store($path);
		$image = $request->file('image');
		$filename= time().'.'.$image->getClientOriginalExtension();
		$request->image->move(public_path('gambar'), $filename);
      $post = DB::table('profiles')->where('user_id', Auth::user()->id);
      $post->update(['file_name' => '/gambar/'.$filename]);
      return redirect('/profile/'.Auth::user()->id);
	}
}
