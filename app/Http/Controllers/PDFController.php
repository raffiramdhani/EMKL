<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use PDF;

class PDFController extends Controller
{

	public function ReportCB(Request $req){

		// set document information
		PDF::SetCreator('Project STB.DEV');
		PDF::SetAuthor('SuperAdmin');
		PDF::SetTitle('Report Project STB');
		PDF::SetSubject('Laporan');
		PDF::SetKeywords('TCPDF, PDF, Custom Bond');

		PDF::SetMargins('14', '55', '14');
		PDF::SetAutoPageBreak(TRUE, '30');

		PDF::setFont('dejavusans','','6.8');

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    PDF::setLanguageArray($l);
		}

		
		PDF::AddPage();

		$html = '<div style="text-align:center;font-size:9"><b>N o m o r : '.$req->no.'</b></div>
		<br/><br/>Yang bertanda tangan dibawah ini :<br/>';
		$html .= '<table border="0" cellspacing="1" cellpadding="0" style="font-size:7">
					<tr>
						<td width="160px"> &nbsp; &nbsp; Nama</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Jabatan</td>
						<td>: </td>
					</tr>
				</table>';
		$html .= '<br/>Dalam hal ini bertindak untuk dan atas nama :<br/>';
		$html .= '<table border="0" cellspacing="1" cellpadding="0" style="font-size:7">
					<tr>
						<td width="160px"> &nbsp; &nbsp; Nama Perusahaan</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Berkedudukan di</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; NPWP</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Alamat</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Telepon</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Faksimili dan <i>E-mail</i></td>
						<td>: </td>
					</tr>
				</table>';
		$html .= '<br/>Yang selanjutnya disebut <i>Surety</i>,<br/>Berjanji dan menjamin :<br/>';
		$html .= '<table border="0" cellspacing="1" cellpadding="0" style="font-size:7">
					<tr>
						<td width="160px"> &nbsp; &nbsp; Nama</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; NPWP</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; NIPER</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Alamat</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Telepon</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Faksimili</td>
						<td>: </td>
					</tr>
				</table>';
		$html .= '<br/>Yang selanjutnya disebut <i>Principal</i>,<br/>
		Dengan melepaskan hak istimewa untk menuntut supaya barang-barang <i>Principal</i> lebih dahulu disita dan dijual untuk melunasi hutang-hutangnya<br/>yang oleh undang-undang diberikan kepada <i>Surety</i> sesuai dengan Pasal 1832 Kitab Undang-undang Hukum Perdata, termasuk juga haknya untuk<br/>terlebih dahulu mendapat pembayaran piutang, akan membayar segera dan sekaligus kepada <b>{0}</b><br/>(yang selanjutnya disebut <i>Obligee</i>) uang paling banyak sebesar <b>Rp.{1}</b>, <br/>apabila <i>Principal</i> tidak dapat memenuhi kewajiban pabean atas :<br/> ';
		$html .= '<br/><br/><table border="0" cellspacing="1" cellpadding="0" style="font-size:7">
					<tr>
						<td width="160px"> &nbsp; &nbsp; Kegiatan Kepabeanan</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"> &nbsp; &nbsp; Dokumen Sumber</td>
						<td>: </td>
					</tr>
					<tr>
						<td width="160px"></td>
						<td>: </td>
					</tr>
				</table>';
		$html .= '<br/><br/>Klaim atas <i>Custom Bond</i> ini harus telah selesai diajukan oleh <i>Obligee</i> dan diterima oleh <i>Surety</i> dalam jangka waktu paling lama 30 (tiga puluh) hari<br/>sejak tanggal jatuh tempo <i>Custom Bond</i> ini dengan menggunakan Surat Pencarian Jaminan..<br/>Pembayaran atas klaim <i>Custom Bond</i> ini dilakukan paling lama 6 (enam) hari kerja sejak tanggal diterimanya Surat Pencairan Jaminan dengan<br/>ketentuan:<br/>a.&emsp;disetorkan ke Kas Negara sejumlah yang tertera dalam Surat Pencairan Jaminan; dan <br/>b.&emsp;apabila terdapat sisa dari penyetoran tersebut pada huruf a, dikembalikan kepada <i>Principal</i><br/>Apabila sampai dengan tanggal jatuh tempo klaim <i>Custom Bond</i>, <i>Surety</i> tidak menerima Surat Pencairan Jaminan dari <i>Obligee</i>, <i>Surety</i> tidak<br/>bertanggung jawab atas pembayaran dimaksud (batal demi hukum tanpa menghilangkan tagihan negara kepada <i>Principal</i>).<br/>Penyesuaian Jaminan hanya dapat dilakukan setelah mendapat persetujuan Kepala Kantor Pabean.<br/><i>Custom Bond</i> ini berlaku terhitung mulai <b>{2}</b> sampai dengan tanggal <b>{3}</b> (jatuh tempo <i>Custom Bond</i>).<br/>Dibuat dan ditandatangani di Jakarta pada tanggal <b>{4}</b>.';
		$html .= '<br/><br/><br/><br/><div style="text-align:right;font-size:9"><b>{5}</b><br/><br/><br/><br/><br/><br/><br/><div style="text-decoration:underline"><b>{6}</b></div><br/><b>{7}</b></div>';

		PDF::writeHTML($html, true, false, true, false, '30px');
		// output the HTML content
		
		PDF::Output('example_006.pdf', 'I');
		return view('template.getpdf');

	}

	public function ReportPL(){

		PDF::SetCreator('Project STB.DEV');
		PDF::SetAuthor('SuperAdmin');
		PDF::SetTitle('Report Project STB');
		PDF::SetSubject('Laporan');
		PDF::SetKeywords('TCPDF, PDF, Custom Bond');

		PDF::SetMargins('24', '14', '14');
		PDF::SetAutoPageBreak(TRUE, '30');

		PDF::setFont('dejavusans','','10');

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    PDF::setLanguageArray($l);
		}

		
		PDF::AddPage('L');

		$html = '<div style="text-align:center"><div style="font-family:times;font-size:20"><b>INVOICE</b></div>
				<br/>No.:0239/STB-TKP-INV/111/2018</div>';
		$html .= '<br/><br/>
					<table border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="130px">Nama Principal &emsp; &emsp; &nbsp; :</td>
							<td style="text-align:center;width:550px">PT.MICRO GARMENT<br/>Jl.Raya Rancaekek - Majalaya No.389, Solokan Jeruk, Majalaya, Jawa Barat<br/><br/></td>
						</tr>
						<tr>
							<td width="130px">Pembayaran untuk &emsp; :</td>
							<td width="120px">Custom Bond No.</td>
							<td width="10px">:</td>
							<td style="text-align:center;width:310px">TKP.01505.18.0500239</td>
						</tr>
						<tr>
							<td width="130px"></td>
							<td width="120px">Atas PIB No.</td>
							<td width="10px">:</td>
							<td style="text-align:center;width:310px">000000-005276-20180319-010282</td>
						</tr>
						<tr>
							<td width="130px"></td>
							<td width="120px">Nilai Jaminan</td>
							<td width="50px">: &emsp;Rp.</td>
							<td style="text-align:right;width:120px">36.456.000</td>
						</tr>
					</table>';
		$html .= '<br/><br/>
					<table border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="130px">Dengan jumlah &emsp; &emsp; &nbsp; :</td>
							<td width="120px">Nilai Premi</td>
							<td width="50px">: &emsp;Rp.</td>
							<td style="text-align:right;width:120px">401.016</td>
						</tr>
						<tr>
							<td width="130px"></td>
							<td width="120px">Polis & Material</td>
							<td width="10px">:</td>
							<td width="35px" style="border-bottom:1;text-align:center"> Rp.</td>
							<td style="text-align:right;width:125px;border-bottom:1">30.000</td>
						</tr>
						<tr>
							<td width="130px"></td>
							<td width="120px">Total Biaya</td>
							<td width="10px">:</td>
							<td width="160"></td>
							<td width="35px" style="border-bottom:1;text-align:center"> Rp.</td>
							<td style="text-align:right;width:120px;border-bottom:1">431.016</td>
						</tr>
						<tr>
							<td width="130px"></td>
							<td width="120px" style="text-align:right">Terbilang</td>
							<td width="10px">:</td>
							<td style="text-align:center;width:400px;height:30px">&nbsp;<br/><b># Empat Ratus Tiga Puluh Satu Ribu Enam Belas Rupiah #</b></td>
						</tr>
					</table>';
			$html .= '<br/><br/>
						<table border="0" cellspacing="0" cellpadding="3">
							<tr>
								<td width="250px">Bank Mandiri KCP.Bdg Buah Batu,</td>
								<td width="130px"></td>
								<td width="300px">Bandung, 22 Maret 2018</td>
							</tr>
							<tr>
								<td width="250px">No.Rekening : 1300015720991</td>
								<td width="130px"></td>
								<td width="300px">Hormat Kami,</td>
							</tr>
							<tr>
								<td width="250px">Atas Nama : PT.Solusi Teknis Bandung</td>
								<td width="130px"></td>
								<td width="300px" style="text-align:center">PT.Solusi Teknis Bandung<br/><br/><br/><br/><br/><br/><br/><br/><b><u>Anang Sungkawa</u></b></td>
							</tr>
						</table>';
		PDF::writeHTML($html, true, false, true, false, '30px');
		PDF::Output('example_006.pdf', 'I');
		return view('template.getpdf');
	}

	public function ReportPIB(){

		PDF::SetCreator('Project STB.DEV');
		PDF::SetAuthor('SuperAdmin');
		PDF::SetTitle('Report Project STB');
		PDF::SetSubject('Laporan');
		PDF::SetKeywords('TCPDF, PDF, Custom Bond');

		PDF::SetMargins('14', '40', '14');
		PDF::SetAutoPageBreak(TRUE, '30');

		PDF::setFont('dejavusans','','8.5');

		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	    require_once(dirname(__FILE__).'/lang/eng.php');
	    PDF::setLanguageArray($l);
		}

		PDF::AddPage();

		$html = '<b><u>Jakarta, 22 Maret 2018</u></b><br/>
				No.0239/CB/BP/III/18/01<br/><br/>
				Kepada<br/>
				<b>PT.ASURANSI TUGU KRESNA PRATAMA</b><br/>
				Jl.Raya Pasar Minggu No.5, Jakarta Selatan 12780<br/><br/>
				UP : Yth, Ibu Ir.NOVY TANINA, AAAIK<br/><br/>
				Dengan hormat,<br/>
				Bersama ini kami mohon ketersediaan Bapak/Ibu untuk melaksanakan penutupan Asuransi Custom Bond, dengan data-<br/>
				data sebagai berikut :';
		$html .= '<br/><br/>
					<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td width="160px">Nama Perusahaan</td>
							<td width="360px">: PT.MICRO GARMENT</td>
						</tr>
						<tr>
							<td width="160px">NPWP</td>
							<td>: 02.192.925.2-444.000</td>
						</tr>
						<tr>
							<td width="160px">Alamat</td>
							<td>: Jl.Raya Rancaekek Majalaya No.389, Solokan Jeruk, Majalaya<br/>&nbsp; Jawa Barat</td>
						</tr>
						<tr>
							<td width="160px">NIPER</td>
							<td>: 150520/254/KW.08/6061</td>
						</tr>
						<tr>
							<td width="160px">Telepon</td>
							<td>: 022 - 5950531</td>
						</tr>
						<tr>
							<td width="160px">Faksimili</td>
							<td>: 022 - 5955164</td>
						</tr>
						<tr>
							<td width="160px">Kantor Pabean</td>
							<td>: KPU BEA DAN CUKAI TANJUNG PRIOK</td>
						</tr>
						<tr>
							<td width="160px">Kegiatan Pabean</td>
							<td>: KITE</td>
						</tr>
						<tr>
							<td width="160px">Dokumen Sumber</td>
							<td>:</td>
						</tr>
					</table>';
		$html .= '<br/><br/>
					<table border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="15px"></td>
							<td width="145px">No & Tanggal Kontrak Kerja</td>
							<td>:</td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td width="145px">No & Tanggal BC</td>
							<td>:</td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td width="145px">No & Tgl SK Fasilitas Bintek</td>
							<td>: <br/></td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td width="145px">No & Tgl Register Bintek</td>
							<td>: 150520/254/KW.08/6061 &emsp; Tanggal. 20/05.2015<br/></td>
						</tr>
						<tr>
							<td width="15px"></td>
							<td width="145px">No & PIB & Tgl PIB</td>
							<td>: 000000-005276-20180319-010282</td>
						</tr>
					</table>';
		$html .= '<br/><br/>
					<table border="0" cellspacing="0" cellpadding="3">
						<tr>
							<td width="160px">Jangka Waktu Jaminan</td>
							<td>:</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td width="550px">Untuk keperluan penagguhan pembayaran Bea Masuk dan PPN atas barang-barang import sebagai berikut :</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td width="160px">Bea Masuk</td>
							<td width="50">: Rp.</td>
							<td width="150" style="text-align:right">11.760.000,00</td>
						</tr>
						<tr>
							<td width="160px">Denda Administrasi</td>
							<td width="50">: Rp.</td>
							<td width="150" style="text-align:right">0,00</td>
						</tr>
						<tr>
							<td width="160px">PPN</td>
							<td width="50">: Rp.</td>
							<td width="150" style="text-align:right">24.696.000,00</td>
						</tr>
						<tr>
							<td width="160px">PPh Pasal 22</td>
							<td width="50">: Rp.</td>
							<td width="150" style="text-align:right">0,00</td>
						</tr>
						<tr>
							<td width="360" style="border-top:1">
							</td>
						</tr>
					</table>';

		PDF::writeHTML($html, true, false, true, false, '30px');
		PDF::Output('example_006.pdf', 'I');
		return view('template.getpdf');

	}

	// public function Header(){
	// 	PDF::SetFont('helvetica', 'B', 20);
 //        // Title
 //        PDF::Cell(0, 15, '<< TCPDF Example 003 >>', 0, false, 'C', 0, '', 0, false, 'M', 'M');
	// }

  //   public function ExportPDF(){

		// // set document information
		// PDF::SetCreator('PDF_CREATOR');
		// PDF::SetAuthor('Nicola Asuni');
		// PDF::SetTitle('Report PDF');
		// PDF::SetSubject('TCPDF Tutorial');
		// PDF::SetKeywords('TCPDF, PDF, example, test, guide');

		// // set default header data
		// PDF::SetHeaderData('PDF_HEADER_LOGO', 'PDF_HEADER_LOGO_WIDTH', 'PDF_HEADER_TITLE'.' 011', 'PDF_HEADER_STRING');

		// // set default monospaced font
		// PDF::SetDefaultMonospacedFont('PDF_FONT_MONOSPACED');

		// // set margins
		// PDF::SetMargins('PDF_MARGIN_LEFT', 'PDF_MARGIN_TOP', 'PDF_MARGIN_RIGHT');
		// PDF::SetHeaderMargin('PDF_MARGIN_HEADER');
		// PDF::SetFooterMargin('PDF_MARGIN_FOOTER');

		// // set auto page breaks
		// PDF::SetAutoPageBreak('TRUE', 'PDF_MARGIN_BOTTOM');

		// // set image scale factor
		// PDF::setImageScale('PDF_IMAGE_SCALE_RATIO');

		// // set some language-dependent strings (optional)
		// if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
		//     require_once(dirname(__FILE__).'/lang/eng.php');
		//     PDF::setLanguageArray($l);
		// }

		// // ---------------------------------------------------------

		// // set font
		// PDF::SetFont('helvetica', '', 12);

		// // add a page
		// PDF::AddPage();

		// // column titles
		// $header = array('No.','ID', 'Nama Tamu', 'Kontak', 'Alamat');


		// $data = tamu::all();
  //       // Colors, line width and bold font
  //       PDF::SetFillColor(255, 0, 0);
  //       PDF::SetTextColor(255);
  //       PDF::SetDrawColor(128, 0, 0);
  //       PDF::SetLineWidth(0.3);
  //       PDF::SetFont('', 'B');
  //       // Header
  //       $w = array(12, 12, 45, 55, 86);
  //       $num_headers = count($header);
  //       for($i = 0; $i < $num_headers; ++$i) {
  //           PDF::Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
  //       }
  //       PDF::Ln();
  //       // Color and font restoration
  //       PDF::SetFillColor(224, 235, 255);
  //       PDF::SetTextColor(0);
  //       PDF::SetFont('','','8');
  //       // Data
  //       $fill = 0;
  //       $no = 1;
  //       foreach($data as $row) {
  //           PDF::Cell($w[0], 6, $no, 'LR', 0, 'C', $fill);
  //           PDF::Cell($w[1], 6, $row->id, 'LR', 0, 'C', $fill);
  //           PDF::Cell($w[2], 6, $row->NamaTamu, 'LR', 0, 'L', $fill);
  //           PDF::Cell($w[3], 6, $row->NoKontak, 'LR', 0, 'C', $fill);
  //           PDF::Cell($w[4], 6, $row->Alamat, 'LR', 0, 'L', $fill);
  //           PDF::Ln();
  //           $fill=!$fill;
  //           $no++;
  //       }
  //       PDF::Cell(array_sum($w), 0, '', 'T');

		// // close and output PDF document
		// PDF::Output('example_011.pdf', 'I');

		// //============================================================+
		// // END OF FILE
		// //============================================================+
  //   }
}
