<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
use App\Role;
use App\PIB;
use Auth;
use PDF;
use DB;

class ViewsController extends Controller
{

	public function show(){
		return view('template.welcome');
	}

	public function index(){
		return view('template.home');
	}

    public function info(){
    	$tamus = Tamu::all();
    	return view('template.ginfo', ['tamus' => $tamus]);
    }
	
	public function getreport(){
		return view('template.getpdf');
	}

	public function getData(){
		$data1 = DB::table('tamus')->join('reservations','reservations.tamu_id','=', 'tamus.id')
							->select('tamus.*', 'reservations.*')->get();
		$data = array('data'=> $data1);
		return response()->json($data);
	}

	public function getData2(){
		$data2 = keperluan::join('tamus','tamus.id', '=', 'keperluans.tamu_id')
		->select('tamus.*', 'keperluans.*')
		->get();
		$data = array('data' => $data2);
		return response()->json($data);
	}

	public function getPIBData(){
		$data_fetch = DB::table('p_i_b_s')->join('kliens','p_i_b_s.id_cln', '=', 'kliens.id_cln')
						->select('p_i_b_s.*', 'kliens.*')->get();
		$data = array('data' => $data_fetch);
		return response()->json($data);
	}

	public function upview(){
		return view('template.upload');
	}

	public function proccess($type){
		return view('template.proses'.$type);
	}

	public function database($type){
		return view('template.database-'.$type);
	}

	public function report($type){
		return view('template.report'.$type);
	}

	public function setting(){
		return view('template.pengaturan');
	}

	public function getClient(){
		$data_fetch = DB::table('kliens')->select('kliens.*')->get();
		$data = array('data' => $data_fetch);
		return response()->json($data);
	}

	public function getAsuransi(){
		$data_fetch = DB::table('asuransis')->select('asuransis.*')->get();
		$data = array('data' => $data_fetch);
		return response()->json($data);
	}

	public function getCB(){
		$data_fetch = DB::table('cbs')
						->join('p_i_b_s', 'cbs.id_pib' ,'=' ,'p_i_b_s.id_pib')
						->select('cbs.*', 'p_i_b_s.*')->get();
		$data = array('data' => $data_fetch);
		return response()->json($data);
	}

}
