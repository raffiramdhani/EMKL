<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use App\User;
use App\Profile;
use App\Role;
use Auth;

class UserController extends Controller
{
    public function imgStore(Request $request,$id){
		$profile = new Profile();
		if(Input::hasFile('image')){
			$file = Input::file('image');
			$file->move(public_path().'/', $file->getClientOriginalName());
			$profile->file_name = $file->getClientOriginalName();
			$profile->size = $file->getClientSize();
			$profile->type = $file->getClientMimeType();
		}
		$profile->save();
		return redirect('/profile/'+$id);
	}

	public function create_profile($id, Request $request){
		$profile = DB::transaction(function() use ($id, $request){
			DB::table('users')->where('id',$id)->update([
				'name' => $request->name,
				'email' => $request->mail,
			]);
			DB::table('profiles')->where('user_id',$id)->update([
				'address' => $request->address,
				'education' => $request->edu,
				'job' => $request->job,
			]);
		});
		return redirect('/home');
	}

	public function user_index(){
		$users = DB::table('users')
						 ->join('roles','roles.id','=', 'users.id')
						 ->join('role_user', 'role_user.user_id', '=', 'users.id')
						 ->select('users.*', 'roles.description', 'role_user.*')
						 ->get();
		$data = array('data' => $users);
		return response()->json($data);
	// 	// return dd(['users' =>$users, 'roles' => $role]);
	}

	public function getData3(){
		$data3 = User::join('role_user','role_user.user_id','=','users.id')
		->join('roles','roles.id','=','role_user.role_id')
		->select('users.*','role_user.*','roles.description')
		->get();
		$data = array('data' => $data3);
		return response()->json($data);
	}

	public function profile($id){
		$profiles = Profile::find($id);
		return view('template.profile', ['profiles' => $profiles]);
	}

	public function user_delete($id){
		User::where('id',$id)->delete();
		return response()->json();
	}

	public function user_change($id, Request $req){
		if(Auth::User()->hasRole('superadministrator')){
			$change = DB::transaction(function() use($id,$req){
				DB::table('users')
				->where('id',$id)
				->update(['name' => $req->name,
						  'email' => $req->mail,]);
				DB::table('role_user')
				->where('user_id',$id)
				->update(['role_id' => $req->role]);
			});
			return response()->json($change);
		}
		else{
			$change = DB::table('role_user')
			->where('user_id',$id)
			->update(['role_id' => $req->role]);

			return response()->json($change);
		}
	}

}
