<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleDefiner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::User()->hasRole('superadministrator'))
            return $next($request);
        elseif(Auth::User()->hasRole('administrator'))
            return $next($request);

        return redirect('/home');
    }
}
