<?php

namespace App\Listeners;

use App\Events\DataCycle;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class TableInfoCycle
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DataCycle  $event
     * @return void
     */
    public function handle(DataCycle $event)
    {
        //
    }
}
