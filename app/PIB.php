<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PIB extends Model
{
    public function klien(){
    	return $this->belongsToMany('App\Klien');
    }

    public function cb(){
    	return $this->hasMany('App\CB');
    }
}
