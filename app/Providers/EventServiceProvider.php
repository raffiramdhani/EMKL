<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\Event' => [
            'App\Listeners\EventListener',
            'App\Listeners\CreateInformation',
        ],
        'App\Events\UserRegistered' => [
            'App\Listeners\CreateProfile'
        ],
        'App\Events\DataCycle' => [
            'App\Listeners\TableTamuCycle',
            'App\Listeners\TableInfoCycle',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
