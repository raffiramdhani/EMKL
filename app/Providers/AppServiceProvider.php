<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use App\Asuransi;
use App\Blanko;
use App\Invoice;
use App\Klien;
use App\PIB;
use App\Placing;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
        Schema::defaultStringLength(191);
        $asuransi = Asuransi::all();
        $blanko = Blanko::all();
        $invoice = Invoice::all();
        $klien = Klien::all();
        $pib = PIB::all();
        $placing = Placing::all();
        $user = User::all();
        View::share('Asuransi', $asuransi);
        View::share('Blanko', $blanko);
        View::share('Invoice', $invoice);
        View::share('Klien', $klien);
        View::share('PIB', $pib);
        View::share('Placing', $placing);
        View::share('User', $user);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
