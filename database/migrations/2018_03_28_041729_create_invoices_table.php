<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id_inv');
            $table->string('no_inv', 50);
            $table->date('tgl_inv');
            $table->integer('id_plc')->unsigned();
            $table->double('prs_premi');
            $table->double('premi');
            $table->double('polmat');
            $table->integer('rec_usr');
            $table->timestamp('rec_wkt');
            $table->tinyinteger('rec_sta');
            $table->timestamps();

            $table->foreign('id_plc')->references('id_plc')->on('placings')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
