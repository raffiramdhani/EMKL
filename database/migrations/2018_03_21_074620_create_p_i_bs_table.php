<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePIBsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_i_b_s', function (Blueprint $table) {
            $table->increments('id_pib');
            $table->string('no_pib', 50);
            $table->date('tgl_pib');
            $table->integer('id_cln')->unsigned();
            $table->string('type', 10);
            $table->date('tgl_cetak');
            $table->double('bea_masuk');
            $table->double('ppn');
            $table->double('bm_tp');
            $table->double('ppn_tp');
            $table->integer('rec_usr');
            $table->timestamp('rec_wkt');
            $table->tinyinteger('rec_sta');
            $table->timestamps();

            $table->foreign('id_cln')->references('id_cln')->on('kliens')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_i_bs');
    }
}
