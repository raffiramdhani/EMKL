<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAsuransisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asuransis', function (Blueprint $table) {
            $table->increments('id_asrs');
            $table->string('nama_asrs', 100);
            $table->string('png_jwb', 100);
            $table->string('jabatan', 100);
            $table->string('npwp_asrs', 50);
            $table->text('alamat_asrs');
            $table->string('kota_asrs', 100);
            $table->string('telp_asrs', 25);
            $table->string('fax_asrs', 25);
            $table->string('email_asrs', 50);
            $table->tinyinteger('def');
            $table->integer('rec_usr');
            $table->timestamp('rec_wkt');
            $table->tinyinteger('rec_sta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asuransis');
    }
}
