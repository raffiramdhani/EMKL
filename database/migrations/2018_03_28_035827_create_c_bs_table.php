<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCBsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cbs', function (Blueprint $table) {
            $table->increments('id_cb');
            $table->string('no_cb', 50);
            $table->date('tgl_cb');
            $table->integer('id_pib')->unsigned();
            $table->integer('id_asrs')->unsigned();
            $table->string('cb_png_jwb', 100);
            $table->string('cb_jabatan', 100);
            $table->string('beacukai', 100);
            $table->string('kegiatan', 50);
            $table->string('sk', 50);
            $table->date('tgl_sk');
            $table->double('jumlah');
            $table->integer('waktu');
            $table->date('berlaku_m');
            $table->date('berlaku_s');
            $table->string('no_blanko', 10);
            $table->integer('rec_usr');
            $table->timestamp('rec_wkt');
            $table->integer('rec_sta');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cbs');
    }
}
