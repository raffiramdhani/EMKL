<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('placings', function (Blueprint $table) {
            $table->increments('id_plc');
            $table->string('no_plc', 50);
            $table->date('tgl_plc');
            $table->integer('id_cb')->unsigned();
            $table->double('denda');
            $table->double('pph_22');
            $table->integer('rec_usr');
            $table->timestamp('rec_wkt');
            $table->tinyinteger('rec_sta');
            $table->timestamps();

            $table->foreign('id_cb')->references('id_cb')->on('cbs')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('placings');
    }
}
