    // just type in the searchbar what you need to find

$(document).ready(function(){


    // First Timer

    $('#agreed').hide();
    $('#agree').click(function(){
        $('#agreed').show();
        $('#agreeying').hide();
    });

    // End of First Timer 

    var tableUser = $('#TableUser').DataTable({
            "ajax":"/getUser",
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 1,
            }],
            "columns": [
            {bSortable:false,
                data:null,
                className: "center",
                render: function(d){
                    return '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></button><ul class="dropdown-menu" role="menu"><li><a href="#">Another action</a></li><li><a href="#">Something else here</a></li><li class="divider"></li><li><a href="#">Separated link</a></li></ul></div>';
                },},
            {"data":"id"},
            {"data":"name"},
            {"data":"email"},
            {"data":"description"},
            // {bSortable: false,
            //     data:null,
            //     className: "center",
            //     render: function(d){ 
            //         return '<button class="btn btn-primary btn-md editModal" data-id="'+d.id+'" data-nama="'+d.NamaTamu+'" data-kontak="'+d.NoKontak+'" data-alamat="'+d.Alamat+'"><i class="fa fa-cog"></i> Edit</button>  <button class="btn btn-danger btn-md hapusModal" data-id="'+d.id+'"><i class="fa fa-trash"></i> Hapus</button>';
            //     },},
            ],
            "order": [[1, 'desc']],  
    });

    tableUser.on( 'order.dt search.dt', function() {
        tableUser.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell,i){
            cell.innerHTML = i+1;
        });
    }).draw();

    var tableCB = $('#TableCB').DataTable({
            "ajax":"/getCB",
            "lengthMenu": [[25, 50, -1], [25, 50, "All"]],
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 1,
            }],
            "columns": [
            {bSortable:false,
                data:null,
                className: "center",
                render: function(d){
                    return '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'+
                                '<i class="fa fa-cog"></i>'+
                                    '</button><ul class="dropdown-menu" role="menu"><li><a href="#" class="infoCB" data-id="'+d.id_cb+'" data-no_cb="'+d.no_cb+'" data-tgl="'+d.tgl_cb+'" data-klien="'+d.nama_cln+'" data-no_pib="'+d.no_pib+'" data-blanko="'+d.no_blanko+'" data-kegiatan="'+d.kegiatan+'" data-jumlah="'+d.jumlah+'">Another Info</a></li>'+
                                    '<li><a href="#" class="editCB" data-id="'+d.id_cb+'" data-no_cb="'+d.no_cb+'" data-tgl="'+d.tgl_cb+'" data-klien="'+d.nama_cln+'" data-no_pib="'+d.no_pib+'" data-blanko="'+d.no_blanko+'" data-kegiatan="'+d.kegiatan+'" data-jumlah="'+d.jumlah+'">Edit Data</a></li>'+
                                    '<li><a href="#" class="deleteCB" data-id="'+d.id_cb+'">Delete Data</a></li></ul></div>';
                },},
            {"data":"id_cb"},
            {"data":"no_cb"},
            {"data":"tgl_cb"},
            // {"data":"nama_cln"},
            {"data":"no_pib"},
            // {"data":"no_blanko"},
            {"data":"kegiatan"},
            {"data":"jumlah"},
            // {bSortable: false,
            //     data:null,
            //     className: "center",
            //     render: function(d){ 
            //         return '<button class="btn btn-primary btn-md editModal" data-id="'+d.id+'" data-nama="'+d.NamaTamu+'" data-kontak="'+d.NoKontak+'" data-alamat="'+d.Alamat+'"><i class="fa fa-cog"></i> Edit</button>  <button class="btn btn-danger btn-md hapusModal" data-id="'+d.id+'"><i class="fa fa-trash"></i> Hapus</button>';
            //     },},
            ],
            "order": [[1, 'desc']],  
    });

    tableCB.on( 'order.dt search.dt', function() {
        tableCB.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell,i){
            cell.innerHTML = i+1;
        });
    }).draw();

    var TableKlien = $('#TableKlien').DataTable({
            ajax:"/getKlien",
            lengthMenu: [[10, 25, 50, -1],[10, 25, 50, "All"]],
            "columnDefs": [{
                "searchable": false,
                "orderable": false,
                "targets": 0,
            }],
            columns: [
            {bSortable:false,
                data:null,
                className: "center",
                render: function(d){
                    return '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'+
                                '<i class="fa fa-cog"></i>'+
                                    '</button><ul class="dropdown-menu" role="menu"><li><a href="#" class="infoKlien" data-id="'+d.id_cln+'" data-nama="'+d.nama_cln+'" data-npwp="'+d.npwp_cln+'" data-niper="'+d.niper+'" data-tgl="'+d.tgl_sk+'" data-alamat="'+d.alamat_cln+'" data-kota="'+d.kota_cln+'" data-telp="'+d.telp_cln+'" data-fax="'+d.fax_cln+'" data-email="'+d.email_cln+'">Another Info</a></li>'+
                                    '<li><a href="#" class="editKlien" data-id="'+d.id_cln+'" data-nama="'+d.nama_cln+'" data-npwp="'+d.npwp_cln+'" data-niper="'+d.niper+'" data-tgl="'+d.tgl_sk+'" data-alamat="'+d.alamat_cln+'" data-kota="'+d.kota_cln+'" data-telp="'+d.telp_cln+'" data-fax="'+d.fax_cln+'" data-email="'+d.email_cln+'">Edit Data</a></li>'+
                                    '<li><a href="#" class="deleteKlien" data-id="'+d.id_cln+'">Delete Data</a></li></ul></div>';
                },},
            {data:"id_cln"},
            {data:"nama_cln"},
            {data:"npwp_cln"},
            {data:"niper"},
            {data:"tgl_sk"},
            {data:"telp_cln"},
            {data:"fax_cln"},
            {data:"email_cln"},
            ],
            order: [[1, "desc"]],
    });

    TableKlien.on( 'order.dt search.dt', function() {
        TableKlien.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell,i){
            cell.innerHTML = i+1;
        });
    }).draw();

    var TableAsuransi = $('#TableAsuransi').DataTable({
            ajax:"/getAsuransi",
            lengthMenu: [[10, 25, 50, -1],[10, 25, 50, "All"]],
            columns: [
            {bSortable:false,
                data:null,
                className: "center",
                render: function(d){
                    return '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'+
                                '<i class="fa fa-cog"></i>'+
                                    '</button><ul class="dropdown-menu" role="menu"><li><a href="#" class="infoAsuransi" data-id="'+d.id_asrs+'" data-nama="'+d.nama_asrs+'" data-npwp="'+d.npwp_asrs+'" data-png="'+d.png_jwb+'" data-jabat="'+d.jabatan+'" data-alamat="'+d.alamat_asrs+'" data-kota="'+d.kota_asrs+'" data-telp="'+d.telp_asrs+'" data-fax="'+d.fax_asrs+'" data-email="'+d.email_asrs+'">Another Info</a></li>'+
                                    '<li><a href="#" class="editAsuransi" data-id="'+d.id_asrs+'" data-nama="'+d.nama_asrs+'" data-npwp="'+d.npwp_asrs+'" data-png="'+d.png_jwb+'" data-jabat="'+d.jabatan+'" data-alamat="'+d.alamat_asrs+'" data-kota="'+d.kota_asrs+'" data-telp="'+d.telp_asrs+'" data-fax="'+d.fax_asrs+'" data-email="'+d.email_asrs+'">Edit Data</a></li>'+
                                    '<li><a href="#" class="deleteAsuransi" data-id="'+d.id_asrs+'">Delete Data</a></li></ul></div>';
                },},
            {data:"id_asrs"},
            {data:"nama_asrs"},
            {data:"npwp_asrs"},
            {data:"png_jwb"},
            {data:"jabatan"},
            {data:"telp_asrs"},
            {data:"fax_asrs"},
            ],
    });

    TableAsuransi.on( 'order.dt search.dt', function() {
        TableAsuransi.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell,i){
            cell.innerHTML = i+1;
        });
    }).draw();

    var TablePIB = $('#TablePIB').DataTable({
            ajax: "/getPIB",
            lengthMenu: [[10, 25, 50, -1], [10, 20, 50, "All"]],
            columns: [
            {bSortable:false,
                data:null,
                className: "center",
                render: function(d){
                    return '<div class="btn-group"><button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">'+
                                '<i class="fa fa-cog"></i>'+
                                    '</button><ul class="dropdown-menu" role="menu"><li><a href="#" class="infoPIB" data-id="'+d.id_pib+'" data-nopib="'+d.no_pib+'" data-tgl="'+d.tgl_pib+'" data-klien="'+d.nama_cln+'" data-id_cln="'+d.id_cln+'" data-type="'+d.type+'" data-bea_masuk="'+d.bea_masuk+'" data-ppn="'+d.ppn+'" data-bm_tp="'+d.bm_tp+'" data-ppn_tp="'+d.ppn_tp+'">Another Info</a></li>'+
                                    '<li><a href="#" class="editPIB" data-id="'+d.id_pib+'" data-noPIB="'+d.no_pib+'" data-tgl="'+d.tgl_pib+'" data-id_cln="'+d.id_cln+'" data-type="'+d.type+'" data-bea_masuk="'+d.bea_masuk+'" data-ppn="'+d.ppn+'" data-bm_tp="'+d.bm_tp+'" data-ppn_tp="'+d.ppn_tp+'">Edit Data</a></li>'+
                                    '<li><a href="#" class="deletePIB" data-id="'+d.id_pib+'">Delete Data</a></li></ul></div>';
                },},
            {data:"id_pib"},
            {data:"no_pib"},
            {data:"tgl_pib"},
            {data:"nama_cln"},
            {data:"type"},
            {data:"bea_masuk"},
            {data:"ppn"},
            {data:"bm_tp"},
            {data:"ppn_tp"},
            ],
            order: [[0, "desc"]],
    });

    TablePIB.on( 'order.dt search.dt', function() {
        TablePIB.column(1, {search:"applied", order:"applied"}).nodes().each(function(cell,i){
            cell.innerHTML = i+1;
        });
    }).draw();

    $("#nama_error").hide();
    $("#kontak_error").hide();
    $("#alamat_error").hide();  

    $('#dismiss').click(function(event){
            $("#nama_error").hide();
            $("#kontak_error").hide();
            $("#alamat_error").hide();
    });

    $("#NamaTamu").keypress(function(event) {
            var nama = $("#NamaTamu").val();
            var alpha = /^[a-zA-Z-,. ]+$/;
            if(nama.match(alpha)){
              $("#nama_error").hide();
            }else{
              $("#nama_error").html("Shouldn't Contain Any Numeric or Special Symbol").show();
            }
    });

    $('#NamaTamu').focusout(function(event) {
            var nama = $("#NamaTamu").val();
            var alpha = /^[a-zA-Z-., ]+$/;
            if(nama == 0){
                $("#nama_error").html("Shouldn't be Empty").show();
            }else{
              if(nama.match(alpha)){ $("#nama_error").hide(); }
            else{ 
                $("#nama_error").html("Shouldn't Contain Any Numeric or Special Symbol").show();
                }
            }
    });

    $("#NoKontak").focusout(function(event) {
            var kontak = $("#NoKontak").val();
            if(kontak == 0){ $("#kontak_error").html("Shouldn't be Empty").show(); }
            else{ $("#kontak_error").hide(); }
    });

    $("#Alamat").focusout(function(event) {
            var alamat = $("#Alamat").val();
            if(alamat == 0){ $("#alamat_error").html("Shouldn't be Empty").show(); }
            else{ $("#alamat_error").hide(); }
    });

    $('select').change(function(){
            $('#Alamat').val($(this).find(':selected').data('alamat'));
            $('#ident').val($(this).find(':selected').data('id'));
    });

        $(document).on('click','.PIBpage',function(){
                $('.hapusDialog').hide();
                $('.inputTambahText').hide();
                $('.inputTambah').show().removeAttr('readonly');
                $('.inputTambahDate').show().removeAttr('readonly');
                $('.modal-title').text("TAMBAH DATA");
                $('#btnSave').val("add").text("SUBMIT").show();
                $('#myForm').trigger("reset");
                $('#myModal').modal('show');
            });
        $(document).on('click', '.Klienpage', function(){
                $('.hapusDialog').hide();
                $('.inputTambah').show().removeAttr('readonly');
                $('.inputTambahDate').show().removeAttr('readonly');
                $('.inputTambahText').hide();
                $('.modal-title').text("TAMBAH DATA");
                $('#btnSave').val("new").text("SUBMIT").show();
                $('#myForm').trigger("reset");
                $('#myModal').modal('show');
        });
        $(document).on('click', '.Asuransipage', function(){
                $('.hapusDialog').hide();
                $('.inputTambah').show().removeAttr('readonly');
                $('.inputTambahDate').show().removeAttr('readonly');
                $('.inputTambahText').hide();
                $('.modal-title').text("TAMBAH DATA");
                $('#btnSave').val("baru").text("SUBMIT").show();
                $('#myForm').trigger("reset");
                $('#myModal').modal('show');
        });

        $(document).on('click', '.CBpage', function(){
                $('.hapusDialog').hide();
                $('.inputTambah').show().removeAttr('readonly');
                $('.inputTambahDate').show().removeAttr('readonly');
                $('.inputTambahText').hide();
                $('.modal-title').text("TAMBAH DATA");
                $('#btnSave').val("tambah").text("SUBMIT").show();
                $('#myForm').trigger("reset");
                $('#myModal').modal('show');
        });

        $(document).on('click', '.infoPIB', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambahDate').hide();
            $('.inputTambah').show().attr('readonly','readonly');
            $('.inputTambahText').show().attr('readonly','readonly');
            $('.modal-title').text('INFORMASI');
            $('#btnSave').hide();
            $('#identifier').text($(this).data('klien'));
            $('#noPIB').val($(this).data('nopib'));
            $('#TglPIBText').val($(this).data('tgl'));
            $('#type').val($(this).data('type'));
            $('#beaMasuk').val($(this).data('bea_masuk'));
            $('#ppn').val($(this).data('ppn'));
            $('#bmTP').val($(this).data('bm_tp'));
            $('#ppnTP').val($(this).data('ppn_tp'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.infoKlien', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambah').show().attr('readonly','readonly');
            $('.inputTambahText').show().attr('readonly','readonly');
            $('.inputTambahDate').hide();
            $('.modal-title').text('INFORMASI');
            $('#btnSave').hide();
            $('#nama_klien').val($(this).data('nama'));
            $('#npwp').val($(this).data('npwp'));
            $('#niper').val($(this).data('niper'));
            $('#tglSK').val($(this).data('tgl'));
            $('#alamat').val($(this).data('alamat'));
            $('#kota').val($(this).data('kota'));
            $('#telepon').val($(this).data('telp'));
            $('#faksimili').val($(this).data('fax'));
            $('#email').val($(this).data('email'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.infoAsuransi', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambah').show().attr('readonly','readonly');
            $('.modal-title').text('INFORMASI');
            $('#btnSave').hide();
            $('#nama_asuransi').val($(this).data('nama'));
            $('#png_jwb').val($(this).data('png'));
            $('#jabatan').val($(this).data('jabat'));
            $('#npwp').val($(this).data('npwp'));
            $('#alamat').val($(this).data('alamat'));
            $('#kota').val($(this).data('kota'));
            $('#telepon').val($(this).data('telp'));
            $('#faksimili').val($(this).data('fax'));
            $('#email').val($(this).data('email'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.infoCB', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambah').show().attr('readonly','readonly');
            $('.inputTambahText').show().attr('readonly','readonly');
            $('.inputTambahDate').hide();
            $('.modal-title').text('INFORMASI');
            $('#btnSave').hide();
            $('#noCB').val($(this).data('no_cb'));
            $('#png_jwb').val($(this).data('png'));
            $('#jabatan').val($(this).data('jabat'));
            $('#npwp').val($(this).data('npwp'));
            $('#alamat').val($(this).data('alamat'));
            $('#kota').val($(this).data('kota'));
            $('#telepon').val($(this).data('telp'));
            $('#faksimili').val($(this).data('fax'));
            $('#email').val($(this).data('email'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.editPIB', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambahDate').show().removeAttr('readonly');
            $('.inputTambah').show().removeAttr('readonly');
            $('.inputTambahText').hide();
            $('.modal-title').text('EDIT DATA');
            $('#btnSave').show().text("SAVE").val("ubah");
            $('#identifier').text($(this).data('klien'));
            $('#noPIB').val($(this).data('nopib'));
            $('#TglPIBText').val($(this).data('tgl'));
            $('#type').val($(this).data('type'));
            $('#beaMasuk').val($(this).data('bea_masuk'));
            $('#ppn').val($(this).data('ppn'));
            $('#bmTP').val($(this).data('bm_tp'));
            $('#ppnTP').val($(this).data('ppn_tp'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.editKlien', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambah').show().removeAttr('readonly');
            $('.inputTambahDate').show().removeAttr('readonly');
            $('.inputTambahText').hide();
            $('.inputTambahtext').hide();
            $('.modal-title').text('EDIT DATA');
            $('#btnSave').show().text("SAVE").val("update");
            $('#nama_klien').val($(this).data('nama'));
            $('#npwp').val($(this).data('npwp'));
            $('#niper').val($(this).data('niper'));
            $('#tanggalSK').val($(this).data('tgl'));
            $('#alamat').val($(this).data('alamat'));
            $('#kota').val($(this).data('kota'));
            $('#telepon').val($(this).data('telp'));
            $('#faksimili').val($(this).data('fax'));
            $('#email').val($(this).data('email'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.editAsuransi', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').hide();
            $('.inputTambah').show().removeAttr('readonly');
            $('.modal-title').text('EDIT DATA');
            $('#btnSave').show().text("SAVE").val("patch");
            $('#nama_asuransi').val($(this).data('nama'));
            $('#png_jwb').val($(this).data('png'));
            $('#jabatan').val($(this).data('jabat'));
            $('#npwp').val($(this).data('npwp'));
            $('#alamat').val($(this).data('alamat'));
            $('#kota').val($(this).data('kota'));
            $('#telepon').val($(this).data('telp'));
            $('#faksimili').val($(this).data('fax'));
            $('#email').val($(this).data('email'));
            $('#myModal').modal('show');
        });

        $(document).on('click', '.deletePIB', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').show();
            $('.inputTambah').hide();
            $('.inputTambahDate').hide();
            $('.inputTambahText').hide();
            $('.modal-title').text('HAPUS DATA');
            $('#btnSave').show().val('hapus').text("CONFIRM");
            $('#myModal').modal('show');
        });

        $(document).on('click', '.deleteKlien', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').show();
            $('.inputTambah').hide();
            $('.inputTambahText').hide();
            $('.inputTambahDate').hide();
            $('.modal-title').text('HAPUS DATA');
            $('#btnSave').show().val('delete').text("CONFIRM");
            $('#myModal').modal('show');
        });

        $(document).on('click', '.deleteAsuransi', function(){
            $('#ident').val($(this).data('id'));
            $('.hapusDialog').show();
            $('.inputTambah').hide();
            $('.modal-title').text('HAPUS DATA');
            $('#btnSave').show().val('remove').text("CONFIRM");
            $('#myModal').modal('show');
        });

        $("#btnSave").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            var state = $('#btnSave').val();
            var formData = {
                klien: $('#klien option:selected').data('id'),
                noPIB: $('#noPIB').val(),
                noCB: $('#noCB').val(),
                idPIB: $('#idPIB option:selected').data('id'),
                nama_klien: $('#nama_klien').val(),
                nama_asrs: $('#nama_asuransi').val(),
                png_jwb: $('#png_jwb').val(),
                jabatan: $('#jabatan').val(),
                no_sk: $('#noSK').val(),
                tanggalPIB: $('#TglPIBDate').val(),
                tanggalCB: $('#TanggalCBDate').val(),
                kegiatan: $('#kegiatan').val(),
                jumlah: $('#jumlah').val(),
                blanko: $('#Blanko').val(),
                type: $('#type').val(),
                beaMasuk: $('#beaMasuk').val(),
                ppn: $('#ppn').val(),
                bmTP: $('#bmTP').val(),
                ppnTP: $('#ppnTP').val(),
                tanggalSK: $('#tanggalSK').val(),
                npwp: $('#npwp').val(),
                niper: $('#niper').val(),
                alamat: $('#alamat').val(),
                kota: $('#kota').val(),
                daerah: $('#daerah').val(),
                telepon: $('#telepon').val(),
                faksimili: $('#faksimili').val(),
                email: $('#email').val(),
            }
            var tablePIB = $('#TablePIB').DataTable();
            var tableKlien = $('#TableKlien').DataTable();
            var tableAsur = $('#TableAsuransi').DataTable();
            var tableCB = $('#TableCB').DataTable();
            console.log(formData);
            switch (state){
                case "add" :
                var type = "POST";
                var my_url = "/insert";
                e.preventDefault();
                    $.ajax({
                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                    console.log(data);

                    tablePIB.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Added New Data', 'Success Alert', {timeOut: 5000});
                    },
                error: function (data) {
                    console.log('Error:', data);
                    toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                }
            });
                    break;
                case "new" :
                    var type = "POST";
                    var my_url = "/insert2";
                    e.preventDefault();
                        $.ajax({
                        type: type,
                        url: my_url,
                        data: formData,
                        dataType: 'json',                
                        success: function (data) {
                        console.log(data);

                        tableKlien.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Added New Data', 'Success Alert', {timeOut: 5000});
                        },
                        error: function (data) {
                        console.log('Error:', data);
                        toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                        }
                });
                    break;
                case "baru" :
                    var type = "POST";
                    var my_url = "/insert3";
                    e.preventDefault();
                        $.ajax({
                        type: type,
                        url: my_url,
                        data: formData,
                        dataType: 'json',                
                        success: function (data) {
                        console.log(data);

                        tableAsur.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Added New Data', 'Success Alert', {timeOut: 5000});
                        },
                        error: function (data) {
                        console.log('Error:', data);
                        toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                        }
                });
                    break; 
                    case "tambah" :
                    var type = "POST";
                    var my_url = "/insert4";
                    e.preventDefault();
                        $.ajax({
                    type: type,
                        url: my_url,
                        data: formData,
                        dataType: 'json',                
                        success: function (data) {
                        console.log(data);

                        tableCB.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Added New Data', 'Success Alert', {timeOut: 5000});
                        },
                        error: function (data) {
                        console.log('Error:', data);
                        toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                        }
                });
                    break;                
                case "update" :
                var id = $('#ident').val();
                var type = "PATCH";
                var my_url = "/change2/"+id;
                e.preventDefault();
                    $.ajax({
                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                    console.log(data);
                    tableKlien.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Changing The Data', 'Success Alert', {timeOut: 5000});
                    },
                error: function (data) {
                    console.log('Error:', data);
                    toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                }
            });
                    break;
                    case "ubah" :
                var id = $('#ident').val();
                var type = "PATCH";
                var my_url = "/change/"+id;
                e.preventDefault();
                    $.ajax({
                    type: type,
                    url: my_url,
                    data: formData,
                    dataType: 'json',
                    success: function (data) {
                    console.log(data);
                    tablePIB.ajax.reload();

                        $('#myForm').trigger('reset');
                        $('#myModal').modal('hide');
                        toastr.success('Successfully Changing The Data', 'Success Alert', {timeOut: 5000});
                    },
                error: function (data) {
                    console.log('Error:', data);
                    toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                }
            });
                    break;
                    case "patch" :
                    var id = $('#ident').val();
                    var type = "PATCH";
                    var my_url = "/change3/"+id;
                    e.preventDefault();
                            $.ajax({
                            type: type,
                            url: my_url,
                            data: formData,
                            dataType: 'json',
                            success: function (data) {
                            console.log(data);
                            tableAsur.ajax.reload();

                                    $('#myForm').trigger('reset');
                                    $('#myModal').modal('hide');
                                    toastr.success('Successfully Changing The Data', 'Success Alert', {timeOut: 5000});
                            },
                    error: function (data) {
                            console.log('Error:', data);
                            toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                    }
                });
                        break;
                    case "delete" :
                    var id = $('#ident').val();
                    e.preventDefault();
                    $.ajax({
                        type: "DELETE",
                        url: "/hapus2/"+id,
                        success: function (data){
                            tableKlien.ajax.reload();
                            $('#myForm').trigger('reset');
                            $('#myModal').modal('hide');
                            toastr.success('Successfully Delete The Data', 'Success Alert', {timeOut: 5000});
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                        }
                    });
                    break;
                    case "hapus" :
                    var id = $('#ident').val();
                    e.preventDefault();
                    $.ajax({
                        type: "DELETE",
                        url: "/hapus/"+id,
                        success: function (data){
                            tablePIB.ajax.reload();
                            $('#myForm').trigger('reset');
                            $('#myModal').modal('hide');
                            toastr.success('Successfully Delete The Data', 'Success Alert', {timeOut: 5000});
                        },
                        error: function (data) {
                            console.log('Error:', data);
                            toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                        }
                    });
                    break;
                    case "remove" :
                    var id = $('#ident').val();
                    e.preventDefault();
                    $.ajax({
                            type: "DELETE",
                            url: "/hapus3/"+id,
                            success: function (data){
                            tableAsur.ajax.reload();
                            $('#myForm').trigger('reset');
                            $('#myModal').modal('hide');
                            toastr.success('Successfully Delete The Data', 'Success Alert', {timeOut: 5000});
                                },
                            error: function (data) {
                            console.log('Error:', data);
                            toastr.error('Unknown Error From Server', 'Error Alert', {timeOut: 5000});
                            }
                    });
                        break;
                }
                    tablePIB.ajax.reload();
                    tableKlien.ajax.reload();
                    tableAsur.ajax.reload();
            });
});



    // Post Page control

    // $('#addRow').click(function(){
    //     var counter = $('#counter').val();
    //     counter++;
    //     $('#limiter').after('<br><input type="text" name="link'+counter+'" class="form-control col-md-2" placeholder="Link Title"><input type="text" name="linkitem'+counter+'" id="limiter" class="form-control col-md-2" placeholder="Link">');
    // });

    // End Of Post Page

    // Header of AdminLTE

    // $(window).scroll(function(){
    //     var scroll = $(window).scrollTop();
    //     if(scroll >= 0){
    //         $('#sdbar').addClass('fixed_sidebar');
    //         $('header').addClass('sticky');
    //     }else{
    //         $('#sdbar').removeClass('fixed_sidebar');
    //         $('header').removeClass('sticky');
    //     }
    // })

    // End of The Header